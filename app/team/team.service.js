(function(){

    angular.module('teamboard.app')
            .service('TeamService', TeamService);

    //////////////////////////////////////////
    TeamService.$inject = ['$q','$http','API_BASE_URL','DEFAULT_ERROR_CALLBACK'];
    function TeamService($q, $http, API_BASE_URL, DEFAULT_ERROR_CALLBACK) {
        this.findAllTeams = findAllTeams;
        this.findTeam = findTeam;
        this.update = updateTeam;
        this.save = saveTeam;
        this.delete = deleteTeam;

        //////////////////
        function findAllTeams() {
            var TEAMS = 'TEAMS';
            var STATS = 'STATS';
            var combineTeamsWithTheirStats = function (responses) {
                var teams = responses[TEAMS].data.map(function (team) {
                    team.stats = responses[STATS].data[team.id];
                    return team;
                })
                return teams;
            };

            var requests = {TEAMS:$http.get(API_BASE_URL+'/teams'), STATS: $http.get(API_BASE_URL+'/teams/stats')};
            return $q.all(requests)
                .then(combineTeamsWithTheirStats)
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function findTeam(teamId) {
            return $http
                .get(API_BASE_URL+'/teams/'+teamId)
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function updateTeam(team) {
            return $http
                .put(API_BASE_URL+'/teams',team)
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function saveTeam(team) {
            console.log(team);
            return $http
                .post(API_BASE_URL+'/teams',team)
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function deleteTeam(team) {
            return $http
                .delete(API_BASE_URL+'/teams'.concat('/',team.id))
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }

    }

})();

