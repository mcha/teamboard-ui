(function(){

    var teams = {
        templateUrl: 'app/team/teams.html',
        controller: TeamsController,
        controllerAs: 'vm'
    };

    var exampleTeams = [
        {id: 1, name: 'Team Rocket'},
        {id: 2, name: 'Alpha Team'},
        {id: 3, name: 'Red Team'}
    ];

    angular
        .module('teamboard.app')
        .component('teams', teams);

    //////////////////////////////////////////
    
    TeamsController.$inject = ['TeamService','DEFAULT_ERROR_CALLBACK'];
    function TeamsController(TeamService, DEFAULT_ERROR_CALLBACK) {
        this.$onInit = function() {
            // this.teams = exampleTeams;
            this.teams = [];
            this.findAllTeams = findAllTeams;
            this.update = updateTeam;
            this.save = saveTeam;
            this.delete = deleteTeam;
            this.reset = reset;
            this.reset();
        }

        //////////////////
        function reset() {
            this.newTeam = {name : ""};
            this.findAllTeams();
        }

        function findAllTeams() {
            var whenSuccess = (function (teams) {
                this.teams = teams;
            }).bind(this);

            TeamService
                .findAllTeams()
                .then(whenSuccess)
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function updateTeam(team) {
            var whenSuccess = (function (updatedTeamId) {
                this.reset()
                return updatedTeamId;
            }).bind(this);
            
            TeamService
                .update(team)
                .then(whenSuccess)
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function saveTeam(team) {
            var whenSuccess = (function (newTeamId) {
                this.reset();
                return newTeamId;
            }).bind(this);

            TeamService
                .save(team)
                .then(whenSuccess)
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function deleteTeam(team) {
            var whenSuccess = (function () {
                this.reset();
            }).bind(this);

            TeamService
                .delete(team)
                .then(whenSuccess)
                .catch(DEFAULT_ERROR_CALLBACK);
        }
    }
    
})();

