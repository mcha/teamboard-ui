(function(){

    angular.module('teamboard.app')
            .config(routes);

    //////////////////////////////////////////

    routes.$inject = ['$stateProvider','$urlRouterProvider'];
    function routes($stateProvider, $urlRouterProvider) {
        var views = {
            'title':{ template: '<h1 class="text-white">Teamboard</h1>' },
            'content': { component: 'teams' }
        };

        var teamState = {
            url: '/teams',
            views: views
        };

        $stateProvider.state('teams', teamState);
    }
    
})();

