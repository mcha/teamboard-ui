(function(){

    angular.module('teamboard.app')
            .config(routes);

    //////////////////////////////////////////
    
    routes.$inject = ['$urlRouterProvider'];
    function routes($urlRouterProvider) {
        $urlRouterProvider.otherwise('/teams');
    }
    
})();

