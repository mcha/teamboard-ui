(function(){

    angular.module('teamboard.app')
            .service('TaskService', TaskService);

    //////////////////////////////////////////
    TaskService.$inject = ['$http', 'API_BASE_URL','DEFAULT_ERROR_CALLBACK'];
    function TaskService($http, API_BASE_URL, DEFAULT_ERROR_CALLBACK) {
        this.update = updateTask;
        this.save = saveTask;
        this.delete = deleteTask;
        this.findTeamTasks = findTeamTasks;

        //////////////////
        function updateTask(task) {
            return $http
                .put(API_BASE_URL+"/tasks",task)
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function saveTask(task) {
            return $http
                .post(API_BASE_URL+"/tasks",task)
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function deleteTask(task) {
            return $http
                .delete(API_BASE_URL+"/tasks/".concat(task.id))
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function findTeamTasks(teamId) {
            return $http
                .get(API_BASE_URL.concat('/teams/',teamId,'/tasks'))
                .then(function (response) {
                    return response.data;
                })
                .catch(DEFAULT_ERROR_CALLBACK);
        }


    }

})();

