(function() {

    var boardTitle = {
        bindings: {
            teamId: '<'
        },
        templateUrl: 'app/board/boardTitle.html',
        controller: BoardTitleController,
        controllerAs: 'vm'
    };

    angular
        .module('teamboard.app')
        .component('boardTitle', boardTitle);


    //////////////////////////////////////////
    BoardTitleController.$inject = ['TeamService','DEFAULT_ERROR_CALLBACK'];
    function BoardTitleController(TeamService, DEFAULT_ERROR_CALLBACK) {
        this.$onInit = function () {
            this.team = {name:''};
            findTeam.call(this, this.teamId);
        };

        //////////////////
        function findTeam(teamId) {
            var whenSuccess = (function (team) {
                this.team = team;
            }).bind(this);

            TeamService
                .findTeam(teamId)
                .then(whenSuccess)
                .catch(DEFAULT_ERROR_CALLBACK);
        }
    }

})();

