(function(){

    angular.module('teamboard.app')
            .config(routes);

    //////////////////////////////////////////

    routes.$inject = ['$stateProvider'];
    function routes($stateProvider) {
        var views = {
            'title': { component: 'boardTitle'},
            'content': { component: 'board'}
        };
        
        var boardState = {
            url: '/teams/:teamId/board',
            views: views,
            resolve: { teamId: teamIdFromPath }
        };

        $stateProvider.state('board', boardState);

        ////////////////
        teamIdFromPath.$inject = ['$transition$'];
        function teamIdFromPath($transition$) {
            return $transition$.params().teamId;
        }
    }
    
})();

