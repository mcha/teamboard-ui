(function() {

    var taskDetails = {
        bindings: {
            task: '<',
            onUpdate: "&",
            onDelete: "&"
        },
        templateUrl: 'app/board/taskDetails.html',
        controller: TaskDetailsController,
        controllerAs: 'vm'
    };

    angular
        .module('teamboard.app')
        .component('taskDetails', taskDetails);


    //////////////////////////////////////////
    TaskDetailsController.$inject = ['TASK_STATUS'];
    function TaskDetailsController(TASK_STATUS) {
        this.$onInit = function () {
            this.cancel = cancel;
            this.edit = edit;
            this.getNextStatus = getNextStatus;
            this.canMoveNext = canMoveNext;
            this.moveToNextStatus = moveToNextStatus;
            this.isEditing = false;
        }

        //////////////////

        function getNextStatus() {
            switch(this.task.status) {
                case TASK_STATUS.TODO : return TASK_STATUS.WIP;
                case TASK_STATUS.WIP : return TASK_STATUS.DONE;
            }
            return undefined;
        }

        function canMoveNext() {
            return this.getNextStatus()!=undefined;
        }

        function moveToNextStatus() {
            if (this.canMoveNext()) {
                this.task.status = this.getNextStatus();
                this.onUpdate(this.task);
            }
        }

        function cancel() {
            this.isEditing = false;
            Object.assign(this.task, this.taskBackup);
        }

        function edit() {
            this.isEditing = true;
            this.taskBackup = Object.assign({},this.task);
        }
    }

})();

