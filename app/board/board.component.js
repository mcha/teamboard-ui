(function(){

    var board = {
        bindings: {
            teamId: '<'
        },
        templateUrl: 'app/board/board.html',
        controller: BoardController,
        controllerAs: 'vm'
    };

    var TASK_STATUS = {
        TODO:"TODO",
        WIP:"WIP",
        DONE:"DONE"
    };

    angular
        .module('teamboard.app')
        .component('board', board)
        .constant('TASK_STATUS',TASK_STATUS);


    //////////////////////////////////////////
    var exampleTasks = [
        {id: 5, label: 'US-004', description:"Acceptance tests", status:TASK_STATUS.TODO},
        {id: 4, label: 'US-005', description:"Load tests", status:TASK_STATUS.TODO},
        {id: 3, label: 'US-003', description:"Code implemention", status:TASK_STATUS.WIP},
        {id: 2, label: 'US-002', description:"Specification review", status:TASK_STATUS.DONE},
        {id: 1, label: 'US-001', description:"Diagram analysis", status:TASK_STATUS.DONE}];


    BoardController.$inject = ['TaskService','TASK_STATUS','DEFAULT_ERROR_CALLBACK'];
    function BoardController(TaskService, TASK_STATUS, DEFAULT_ERROR_CALLBACK) {
        this.$onInit = function() {
            // this.tasks = exampleTasks;
            this.tasks = [];
            this.findTeamTasks = findTeamTasks;
            this.save = saveTask;
            this.reset = reset;
            this.STATUS_LABELS = Object.values(TASK_STATUS);
            this.reset();
        }

        //////////////////
        function reset() {
            this.newTask = {label : "", description: "", status:"TODO", teamId: this.teamId};
            this.findTeamTasks();
        }

        function findTeamTasks() {
            var whenSuccess = (function (teamTasks) {
                this.tasks = teamTasks;
                return this.tasks;
            }).bind(this);
            
            return TaskService
                .findTeamTasks(this.teamId)
                .then(whenSuccess)
                .catch(DEFAULT_ERROR_CALLBACK);
        }

        function saveTask(task) {
            var whenSuccess = (function (newTaskId) {
                this.reset();
                return newTaskId;
            }).bind(this);

            return TaskService
                .save(task)
                .then(whenSuccess)
                .catch(DEFAULT_ERROR_CALLBACK);
        }

    }
    
})();

