(function() {

    var boardColumn = {
        bindings: {
            name: '@',
            managedStatus: '@',
            tasks: '<',
            onUpdate: '&'
        },
        templateUrl: 'app/board/boardColumn.html',
        controller: BoardColumnController,
        controllerAs: 'vm'
    };

    angular
        .module('teamboard.app')
        .component('boardColumn', boardColumn);


    //////////////////////////////////////////
    BoardColumnController.$inject = ['TaskService','DEFAULT_ERROR_CALLBACK','TASK_STATUS'];
    function BoardColumnController(TaskService, DEFAULT_ERROR_CALLBACK, TASK_STATUS) {
        this.$onInit = function () {
            this.tasksToDiplay = tasksToDiplay;
            this.update = updateTask;
            this.delete = deleteTask;
            initCssClasses.call(this);
        }

        //////////////////
        function initCssClasses() { //private function
            switch(this.managedStatus) {
                case TASK_STATUS.TODO: { this.iconClass = 'fa-diagnoses'; this.headerBgColor = 'bg-primary'; };break;
                case TASK_STATUS.WIP: { this.iconClass = 'fa-cogs'; this.headerBgColor = 'bg-warning'; };break;
                case TASK_STATUS.DONE: { this.iconClass = 'fa-check'; this.headerBgColor = 'bg-success'; };break;
                default: { this.iconClass = 'fa-skull'; this.headerBgColor = 'bg-dark'; };
            }
        }

        function tasksToDiplay() {
            return this.tasks && this.tasks.filter(matchingManagedStatus.bind(this));
            //////////////////
            function matchingManagedStatus(task) {
                return this.managedStatus===task.status;
            }
        }

        function updateTask(task) {
            var whenSuccess = (function () {
                this.onUpdate();
            }).bind(this);

            TaskService
                .update(task)
                .then(whenSuccess).catch(DEFAULT_ERROR_CALLBACK);
        }

        function deleteTask(task) {
            var whenSuccess = (function () {
                this.onUpdate();
            }).bind(this);

            TaskService
                .delete(task)
                .then(whenSuccess).catch(DEFAULT_ERROR_CALLBACK);
        }
    }

})();

