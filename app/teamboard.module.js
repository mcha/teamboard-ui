'use strict';
(function(){
    angular
        .module('teamboard.app', ['ui.router'])
        .constant('API_BASE_URL','http://localhost:9080/api')
        .constant('DEFAULT_ERROR_CALLBACK',function errorCallback(errorDetails) {
            console.error("",errorDetails);
        })
        .config(['$compileProvider', function ($compileProvider) {
            $compileProvider.debugInfoEnabled(true);
        }]);;
})();
